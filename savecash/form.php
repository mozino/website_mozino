
<?php 
$errors = '';
$myemail = 'ricardosavecash@gmail.com';//<-----email
if(empty($_POST['nome'])  || 
   empty($_POST['email']) || 
   empty($_POST['telefone']) ||
   empty($_POST['cnpj']) ||
   empty($_POST['segmento']) ||
   empty($_POST['cidade']) ||
   empty($_POST['estab']))
{
    $errors .= "\n Error: all fields are required";
}

$name = $_POST['nome']; 
$email_address = $_POST['email']; 
$phone = $_POST['telefone'];  
$cnpj = $_POST['cnpj']; 
$segmento = $_POST['segmento']; 
$cidade = $_POST['cidade']; 
$estab = $_POST['estab']; 


if (!preg_match(
"/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", 
$email_address))
{
    $errors .= "\n Error: Invalid email address";
}

if( empty($errors))
{
	$to = $myemail; 
	$email_subject = "SaveCashWebsite - Contato: $name";
	$email_body = "Estabelecimento Beta tester;) ".
	" Detalhes:\n Nome: $name \n Email: $email_address \n Telefone: $phone \n CNPJ \n $cnpj \n Segmento: $segmento \n Cidade: $cidade \n Estabelecimento: $estab"; 
	
	$headers = "From: $myemail\n"; 
	$headers .= "Reply-To: $email_address";
	
	mail($to,$email_subject,$email_body,$headers);
	//redirect to the 'thank you' page
	header('Location: http://www.savecash.com.br');
} 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
	<title>Contact form handler</title>
</head>

<body>
<!-- This page is displayed only if there is some error -->
<?php
echo nl2br($errors);
?>


</body>
</html>